music - Cool and Awesome Sounds
=====

When you think you already find the best collections of music, you are overjoyed and listen to it every time. I found the collections of [Jeff Peterson](http://jeffpeterson.github.io/music/) as I’m researching an essay for [customwritingservice.com](https://www.customwritingservice.com), though the music is rare and the artists are unknown, you will find that his collections are extraordinary. 

The art of the website grabs my attention and spend time listening - Frostbite by Kitty and Friends, Zak Waters, Young Franco Remix, and Milk N Cooks -  are among of the cool music he have.
![screenshot](https://cloud.githubusercontent.com/assets/1184256/9118651/cd741f96-3c26-11e5-9acb-495d970d85b8.png)